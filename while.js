function repetition() {
    let isAgain = true;
    while (isAgain) {
        console.log('I love JS');
        alert('I love JS');
        isAgain = confirm('Coba lagi?');
    }
}

function repetition2() {
    let nilaiAwal = 1;
    while (nilaiAwal <= 10) {
        console.log('I love JS');
        alert('I love JS');
        nilaiAwal++;
        // nilaiAwal = nilaiAwal + 1;
    }
    // Penjelasan
    // deklarasikan nilai awal
    // while(kondisi) {
        // aksi
        // increment / decrement / pengubah nilai
    // }
}

function repetition3() {
    let nilaiAwal = 1;
    let besarLove = 10;

    while (nilaiAwal <= 10) {
        console.log(`I love JS ${besarLove}%`);
        alert(`I love JS ${besarLove}%`);
        nilaiAwal++;
        besarLove = besarLove + 10;
    }
}