function food_checking() {
    let makanan = prompt('Please input a food name!').toLowerCase();

    switch (makanan) {
        case 'sayur':
        case 'buah':
        case 'susu':
        case 'nasi':
            alert('Healthy Food');
            break;
        case 'softdrink':
        case 'burger':
        case 'pizza':
            alert('Unhealthy Food');
            break;
        default:
            alert('Not in food list');
    }
}